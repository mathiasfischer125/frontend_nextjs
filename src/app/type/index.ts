export type Item = {
  id: string;
  text: string;
  createdAt: string;
  updatedAt: string;
};

export type Division = Item & {
  classes: Item[];
};

export type Activity = Item & {
  divisions: Division[];
};

export type ActivityResponse = {
  status: string;
  data: Activity[];
};

export type ActiveData = {
  activity: string;
  division: string;
  classes: string[];
};
