import React from "react";

import "@/styles/globals.scss";

import Header from "@/app/layouts/Header";

const RootLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <Header />
      <div className="container mx-auto mb-10">{children}</div>
    </>
  );
};

export default RootLayout;
