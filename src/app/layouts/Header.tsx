import React from "react";
import Image from "next/image";

import Logo from "../assets/images/logo_wydin.svg";

const Header = () => {
  return (
    <header className="flex h-[50px] flex-col justify-center bg-surface-header md:h-[90px]">
      <div className="container mx-auto flex justify-center md:justify-start">
        <Image className="w-16 md:w-auto" src={Logo} alt="logo" />
      </div>
    </header>
  );
};

export default Header;
