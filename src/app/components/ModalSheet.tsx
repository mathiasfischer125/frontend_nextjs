import React from "react";
import Sheet from "react-modal-sheet";

import ListItem from "@/app/components/ListItem";
import { Item } from "@/app/type";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  classes: Item[];
  activeClasses: string[];
  // eslint-disable-next-line no-unused-vars
  onChangeClass: (id: string) => void;
};

const ModalSheet: React.FC<Props> = ({
  isOpen,
  onClose,
  classes,
  activeClasses,
  onChangeClass,
}) => {
  return (
    <Sheet
      className="pointer-event-none block md:hidden"
      isOpen={isOpen}
      onClose={onClose}
      detent="content-height"
    >
      <Sheet.Container>
        <Sheet.Content className="px-4 pt-7">
          {classes.map((item, index) => (
            <ListItem
              key={index}
              label={item.text}
              multi
              checked={activeClasses.includes(item.id)}
              onClick={() => onChangeClass(item.id)}
            />
          ))}
          <div className="py-7">
            <button
              className="h-10 w-full rounded-3xl border border-gray-400 font-Robot text-sm"
              onClick={onClose}
            >
              Continuer
            </button>
          </div>
        </Sheet.Content>
      </Sheet.Container>
      <Sheet.Backdrop />
    </Sheet>
  );
};

export default ModalSheet;
