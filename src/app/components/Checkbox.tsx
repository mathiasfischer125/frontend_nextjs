import React from "react";

type Props = {
  checked: boolean;
};

const Checkbox: React.FC<Props> = ({ checked }) => {
  return (
    <div className="flex items-center">
      <input
        onChange={() => {}}
        type="checkbox"
        className="absolute h-[14px] w-[15px] opacity-0 md:h-5 md:w-5"
        checked={checked}
      />
      <div
        className="
        mr-2 flex h-[14px] w-[15px] flex-shrink-0 items-center justify-center rounded border border bg-white
         focus-within:border-red-600 hover:cursor-pointer md:h-5 md:w-5"
      >
        <svg
          className="w-3 md:w-4"
          xmlns="http://www.w3.org/2000/svg"
          height="11.252"
          viewBox="0 0 15 11.252"
        >
          <g transform="translate(-1382 -582)">
            <path
              d="M15.14,19.136l-3.751-3.75L9.514,17.261l5.626,5.626,9.374-9.376-1.873-1.877Z"
              transform="translate(1372.486 570.365)"
              fill="#fff"
            />
          </g>
        </svg>
      </div>
    </div>
  );
};

export default Checkbox;
