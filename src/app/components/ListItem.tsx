import React from "react";
import Image from "next/image";

import clsx from "clsx";

import CheckIcon from "@/app/assets/images/check_icon.svg";
import Checkbox from "@/app/components/Checkbox";
import { Division, Item } from "@/app/type";

type Props = {
  recommended?: boolean;
  checked?: boolean;
  label: string;
  multi?: boolean;
  onClick: () => void;
  subOptions?: Division[];
  // eslint-disable-next-line no-unused-vars
  onClickSubOption?: (id: string, options: Item[]) => void;
  activeSubOption?: string;
  // eslint-disable-next-line no-unused-vars
  checkSubOption?: (classes: Item[]) => boolean;
};

const ListItem: React.FC<Props> = ({
  recommended,
  checked = false,
  label,
  onClick,
  multi,
  subOptions = [],
  onClickSubOption = () => {},
  activeSubOption,
  checkSubOption = () => false,
}) => {
  return (
    <>
      <div
        className={clsx(
          "my-0.5 flex py-1.5 hover:cursor-pointer hover:bg-primary-hover md:my-0 md:py-0",
          !multi && checked && "bg-primary-hover"
        )}
        onClick={onClick}
      >
        <p className="flex-1 truncate font-Robot text-xs md:text-body">
          {label}
        </p>
        <div className="items-center md:flex">
          {recommended && <Image className="w-3 md:w-auto" src={CheckIcon} alt="check" />}
          {multi && <Checkbox checked={checked} />}
        </div>
      </div>

      <div className="block pl-5 md:hidden">
        {checked &&
          subOptions.map((item, index) => (
            <div
              key={index}
              className={clsx(
                "my-0.5 py-1.5 hover:cursor-pointer hover:bg-primary-hover flex",
                activeSubOption === item.id && "bg-primary-hover"
              )}
              onClick={() => onClickSubOption(item.id, item.classes)}
            >
              <p className="truncate font-Robot text-xs flex-1">{item.text}</p>
              {checkSubOption(item.classes) && <Image className="w-3 md:w-auto" src={CheckIcon} alt="check" />}
            </div>
          ))}
      </div>
    </>
  );
};

export default ListItem;
