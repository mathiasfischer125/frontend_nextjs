import axios from "axios";

import { ActivityResponse } from "@/app/type";

let API;

export const getActivities = async () => {
  try {
    const { data } = await axios<ActivityResponse>({
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_SERVER}/activities`,
    });
    return data;
  } catch (err) {
    if (err?.message) {
      throw Error(err.message);
    }
  }
};

const getAPI = async () => {
  if (!API) {
    API = getActivities();
  }
  return API;
};

export default getAPI;
