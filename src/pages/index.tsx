import React, { useEffect, useState } from "react";

import { getActivities } from "@/app/api";
import ListItem from "@/app/components/ListItem";
import ModalSheet from "@/app/components/ModalSheet";
import { useMediaQuery } from "@/app/hook/useMediaQuery";
import { ActiveData, Activity, Division, Item } from "@/app/type";

const Home = () => {
  const [activities, setActivities] = useState<Activity[]>([]);
  const [divisions, setDivisions] = useState<Division[]>([]);
  const [classes, setClasses] = useState<Item[]>([]);

  const [active, setActive] = useState<ActiveData>({
    activity: "",
    division: "",
    classes: [],
  });

  const [isOpenClassModal, setIsOpenClassModal] = useState<boolean>(false);
  const isDesktop = useMediaQuery("(min-width: 768px)");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getActivities();
        if (response.data) {
          setActivities(response.data);
        }
      } catch (e) {
        console.log("[Error]", e);
      }
    };

    fetchData();
  }, []);

  const handleChangeActivity = (activityId: string, divisions: Division[]) => {
    if (divisions.length) {
      setDivisions(divisions);
    } else {
      setDivisions([]);
    }
    setClasses([]);
    setIsOpenClassModal(false);
    setActive((prev) => ({
      ...prev,
      activity: prev.activity === activityId ? '' : activityId,
    }));
  };

  const handleChangeDivision = (divisionId: string, classes: Item[]) => {
    if (classes.length) {
      setClasses(classes);
    } else {
      setClasses([]);
    }
    setIsOpenClassModal(!!classes.length);
    setActive((prev) => ({
      ...prev,
      division: divisionId,
    }));
  };

  const handleChangeClasses = (id: string) => {
    const filtered = active.classes.filter((item) => item !== id);
    if (filtered.length === active.classes.length) {
      setActive((prev) => ({
        ...prev,
        classes: [...prev.classes, id],
      }));
    } else {
      setActive((prev) => ({
        ...prev,
        classes: [...filtered],
      }));
    }
  };

  const checkClassesOfDivision = (classes: Item[]) => {
    return classes.some((item) => active.classes.includes(item.id));
  }

  const checkClassesOfActivity = (divisions: Division[]) => {
    return divisions.some((division) => division.classes.some((item) => active.classes.includes(item.id)));
  };

  return (
    <>
      <div className="px-4 pt-5 pb-7 md:px-0 md:py-7">
        <h1 className="font-SerifPro text-title md:text-header">
          Création de compte
        </h1>
      </div>

      <div className="max-w-[1344px] rounded-paper bg-transparent py-0 px-4 shadow-none md:bg-white md:p-10 md:shadow-paper">
        <h2 className="mb-3 font-Robot text-xl font-bold leading-5 md:mb-6 md:text-title">
          Sélectionnez la ou les activités principales de votre entreprise
        </h2>
        <div className="grid grid-cols-1 gap-5 md:grid-cols-3">
          <div>
            <h3 className="mb-2.5 border border-transparent border-b-gray-300 py-2.5 font-SerifPro font-bold text-base md:text-subtitle">
              Activité
            </h3>
            <div>
              {activities.map((item, index) => (
                <ListItem
                  key={index}
                  label={item.text}
                  recommended={checkClassesOfActivity(item.divisions)}
                  checked={active.activity === item.id}
                  onClick={() => handleChangeActivity(item.id, item.divisions)}
                  subOptions={item.divisions}
                  activeSubOption={active.division}
                  checkSubOption={checkClassesOfDivision}
                  onClickSubOption={handleChangeDivision}
                />
              ))}
            </div>
          </div>

          <div className="hidden md:block">
            <h3 className="mb-2.5 border border-transparent border-b-gray-300 py-2.5 font-SerifPro text-base font-bold md:text-subtitle">
              Division
            </h3>
            <div>
              {divisions.map((item, index) => (
                <ListItem
                  key={index}
                  label={item.text}
                  recommended={checkClassesOfDivision(item.classes)}
                  checked={active.classes.includes(item.id)}
                  onClick={() => handleChangeDivision(item.id, item.classes)}
                />
              ))}
            </div>
          </div>

          <div className="hidden md:block">
            <h3 className="mb-2.5 border border-transparent border-b-gray-300 py-2.5 font-SerifPro text-base font-bold md:text-subtitle">
              Classe
            </h3>
            <div>
              {classes.map((item, index) => (
                <ListItem
                  key={index}
                  label={item.text}
                  multi
                  checked={active.classes.includes(item.id)}
                  onClick={() => handleChangeClasses(item.id)}
                />
              ))}
            </div>
          </div>
        </div>
      </div>

      <ModalSheet
        isOpen={!isDesktop && isOpenClassModal}
        onClose={() => setIsOpenClassModal(false)}
        classes={classes}
        activeClasses={active.classes}
        onChangeClass={handleChangeClasses}
      />
    </>
  );
};

export default Home;
