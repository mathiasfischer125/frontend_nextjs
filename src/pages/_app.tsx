import Head from "next/head";

import "@/styles/globals.scss";

import RootLayout from "@/app/layouts";

function MyApp({ Component, pageProps }) {
  return (
    <RootLayout>
      <Component {...pageProps} />
    </RootLayout>
  );
}

export default MyApp;
