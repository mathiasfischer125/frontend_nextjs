module.exports = {
  content: [
    "./src/app/**/*.{js,ts,jsx,tsx}",
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        surface: {
          DEFAULT: "#F8F9F8",
          header: "#051C2C",
          backdrop: "#00000080",
        },
        primary: {
          DEFAULT: "#71DE36",
          hover: "#71DE361A",
        },
      },
      fontFamily: {
        SerifPro: ["Source Serif Pro", "sans-serif"],
        Robot: ["Roboto", "serif"],
        RobotBold: ["Roboto Bold", "serif"],
      },
      fontSize: {
        header: [
          "2.5rem",
          {
            lineHeight: "3.125rem",
            fontWeight: 700,
          },
        ],
        title: [
          "1.75rem",
          {
            lineHeight: "2.125rem",
            fontWeight: 800,
          },
        ],
        subtitle: [
          "1.5rem",
          {
            lineHeight: "1.875rem",
            fontWeight: 800,
          },
        ],
        body: [
          "1rem",
          {
            lineHeight: "2rem",
          },
        ],
      },
      boxShadow: {
        paper: "0px 2px 5px #9BB1A180",
      },
      borderRadius: {
        paper: "10px",
      },
    },
  },
  plugins: [],
};
